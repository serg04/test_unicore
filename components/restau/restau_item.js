import React from "react";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from "react-native";
import { useLinkProps } from "@react-navigation/native";

const imag = {
  uri:
    "https://images.pexels.com/photos/315755/pexels-photo-315755.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
};
const  Restauitem = (props) => {
  return (
    <View style={styles.action_view}>
      
      <View style={styles.action_view2}>
        <Text style={styles.actio_view_text1}>{props.nom}</Text>
        <Text style={styles.actio_view_text2}>XOF {props.prix}</Text>
        {/* <Button style={styles.actio_view_text3} title="text" /> */}
        <TouchableOpacity style={styles.appButtonContainer}>
        <FontAwesome5 style={styles.appButtonText} name={'add'} />
        </TouchableOpacity>
        {/* <Text style={styles.actio_view_text3}>5km, livraison en 24 min </Text> */}
      </View>
      <View style={styles.action_view1}>
        <Image style={styles.image} source={imag} />
      </View>
    </View>
  );
}
export default Restauitem

const styles = StyleSheet.create({
  main_container: {
    height: 190,
    margin: 20,
    flexDirection: "row",
    shadowColor: "#000",
    borderWidth: 1,
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowRadius: 16.0,
    borderRadius: 25,
  },

  appButtonContainer: {
    backgroundColor: "blue",
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10,
    width: "50%",
    height: 30,
    marginTop: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
  },
  image: {
    width: 120,
    height: "100%",
    // margin: 10,
    borderRadius: 25,
    backgroundColor: "gray",
  },

  action_view: {
    // borderWidth: 1,
    alignContent:"center",
    alignContent:"center",
    justifyContent:"center",

    overflow: 'hidden',
    borderRadius:10,
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 10,
    height:150,
    flex: 1,
    flexDirection: "row",
    padding:10,
  },

  action_view1: {
    flex: 1,
    alignItems: "flex-end",
    height:"100%",
    alignSelf:"center"
  },
  action_view2: {
    flex: 1,
    marginLeft: 5,
    alignSelf:"center"
  },

  actio_view_text1: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10,
    marginTop: 3,
    marginBottom: 3,
  },

  actio_view_text2: {
    // fontSize: 10,
    marginLeft: 10,
    marginTop: 2,
    marginBottom: 3,
  },

  actio_view_text3: {
    borderRadius: 25,
    marginLeft: 5,
    marginTop: 2,
    marginBottom: 3,
  },
});
