import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
  TextInput,
  Image,
  Button,
  FlatList,
  TouchableOpacity
} from "react-native";
import { data } from "./../../const/restau_data";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Restauitem from "./restau_item";
import { connect } from "react-redux";

const imag = {
  uri:
    "../../assets/img.jpg",
};

 const Restau = (props) =>{
  return (
    <ScrollView>

   
    <View style={styles.containere}>
      <ImageBackground source={require("./../../assets/slide/resto.jpg")} style={styles.image1}>
     
        <View style={{  flexDirection: 'row' ,alignContent:"center",alignItems:"center",justifyContent:"center", backgroundColor:"white",borderWidth:0,width:"75%",padding:10,borderRadius:10,overflow:"hidden"}}>
        <FontAwesome5 style={styles.appButtonText} name={'arrow-left'} />
        <TextInput placeholder="Search Karim 24" style={styles.TextInput} />
        <FontAwesome5 style={styles.appButtonText} name={'menu'} />
          {/* <View style={styles.main_conten}>
           
          </View> */}
        </View>
      </ImageBackground>

      <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{marginTop:50}}>

                                {
                                    data.map((item) => (
                                        // <View>

                                        //                       <View style={styles.flatList}>
                                        //                 <Text style={styles.textcat}>{item.nom}</Text>
                                        //             <FlatList
                                        //                 data={item.cat}
                                        //                 renderItem={({ item }) => (
                                          <TouchableOpacity style={styles.container}   >
                                          {/* <Icon name={props.icon} tyle={styles.image} /> */}
                                          {/* <Ionicons name={"ios-"+ props.icon} size={25} color="#009387" tyle={styles.image}/> */}
                                          <Image source={require("./../../assets/slide/take-away.svg")}  style={styles.image}/>
                                        {/* <Image style={styles.image} source={{uri:props.img}}/> */}
                                        <Text style={styles.text}>
                                               name
                                            </Text>
                                        
                                     
                                    </TouchableOpacity> //             )}

                                        //             numColumns={3}
                                        //             keyExtractor={(item, index) => index.toString()}
                                        //         />
                                        //     </View>
                                        // </View>

                                    ))
                                }



                            </ScrollView>

      <View style={{ flex: 2 }}>
        <Text style={styles.title_text}>Pizza
        </Text>
        <FlatList
          // data={  props.route.params.menu == "undefined" ? props.route.params.menu : data[0].menu}
          data={   data[0].menu}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) =>
           <Restauitem nom = {item.nom_plat} prix = {item.prix}/>
       
          }
        />
      </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  containere: {
    color: "#fff",
    flex: 1,
  },

  main_conten: {
    marginTop: 50,
    backgroundColor: "#fff",
  },

  image1: {

    resizeMode: "cover",
    justifyContent: "center",
    flexDirection:"column",
    height:200,
    alignItems:"center",
    justifyContent:"center",
    
  },
  action_view: {
    flex: 1,
    flexDirection: "row",
    margin:10,
    justifyContent:"flex-start",
    alignSelf:"flex-start",
    alignItems:"flex-start"
  },
  action_view1: {
    flex: 1,
    alignSelf:"flex-start",
  
  },
  action_view2: {
    flex: 1,
    alignSelf:"center",
    marginLeft:10
  },

  actio_view_text1: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 3,
    marginBottom: 5,
  },

  actio_view_text2: {
    fontSize: 20,
    marginTop: 3,
    marginBottom: 5,
  },

  actio_view_text3: {
    color: "#cca",
    marginTop: 3,
    marginBottom: 5,
  },

  container: {
  height:200,
            backgroundColor: '#fff',
            width: 150,
            justifyContent:"center",
            alignItems:"center",
            marginBottom: 10,
            marginTop: 20,
            marginLeft: 20,
            elevation: 2,
            borderRadius: 15,
            overflow: 'hidden',
            // paddingTop:5
    
    
        },
        text: {
            color: "#009387",
            fontSize: 13,
            fontWeight: "bold",
            textAlign: "center",
            marginBottom: 10,
            marginTop:15
            
    
        },
        image: {
            // flex: 1,
            resizeMode: "cover",
            alignSelf:"center",
            width:100,
            height:100,
            borderRadius:10,
            overflow:"hidden"
          
           
           
    
    
        },
        cat: {
            flex:1,
            marginTop:10,
            justifyContent:"flex-start",
            alignItems:"center"
           
           
    
    
        },

  TextInput: {
    // flex: 3,
    marginLeft: 5,
width:"80%",
    marginTop: 5,
    marginBottom: 5,
    marginRight: 5,
    borderColor: "#000",
    borderWidth: 0,
    paddingLeft: 5,
  },

  title_text: {
    marginTop: 20,
    fontSize: 50,
  },

  button: {
    // flex: 1,
    // width: "10%",
  },
});

// const mapstatprop = ( state ) =>{
//   return state
// }
export default Restau;